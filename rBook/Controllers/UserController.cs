﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace rBook.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        public ActionResult Index()
        {

            return View(CurrentData.UserData);
        }

        [HttpGet]
        public ActionResult Subscribes()
        {
            UserManager manager = new UserManager();
            if (manager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId) != null)
            {
                return View(manager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId));
            }
            else
            {
                rBook.Models.Elofizetesek elof = new Models.Elofizetesek();
                elof.KezdDatum = DateTime.Now;
                elof.LejarDatum = DateTime.Now.AddDays(30);
                return View(elof);
            }
        }

        //[HttpPost]
        public ActionResult NewSubscribe()
        {
            UserManager manager = new UserManager();
            //if (manager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId) != null)
            //{
            //    return View(manager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId));
            //}
            //else
            //{
            //    rBook.Models.Elofizetesek elof = new Models.Elofizetesek();
            //    elof.KezdDatum = DateTime.Now;
            //    elof.LejarDatum = DateTime.Now.AddDays(30);
            //    return View(elof);
            //}
            manager.SetSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId);

            return RedirectToAction("Subscribes");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(rBook.Models.RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                UserManager manager = new UserManager();
                manager.RegisterUser(model);
                manager.ValidateUser(model.Email, model.Password);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(rBook.Models.LoginModel model)
        {
            UserManager manager = new UserManager();
            if (manager.ValidateUser(model.UserName, model.Password))
            {
                return RedirectToAction("Index", "Home");
            }
            else
                return View(model);
        }

        public ActionResult Logout()
        {
            CurrentData.CurrentUser = new UserPrincipal(new UserIdentity());
            foreach (string cookieName in this.ControllerContext.HttpContext.Request.Cookies.AllKeys)
            {
                HttpCookie cookie = this.ControllerContext.HttpContext.Request.Cookies[cookieName];
                //if (!cookieName.Contains("netis.Szimedok.CurrentUICulture"))
                //{
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
                //}
            }
            //Loging.Add(LogConstrains.USERLOGOUT);
            return RedirectToAction("Index", "Home");
        }
    }
}
