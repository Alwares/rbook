﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using rBook.Managers;
using rBook.Models;

namespace rBook.Controllers
{
    public class BooksController : Controller
    {
        //
        // GET: /Books/
        
        public ActionResult Index()
        {
            ContentManager manager = new ContentManager();
            //Aktiv-e az előfizetés
            UserManager usermanager = new UserManager();
            Elofizetesek elof = usermanager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId);
            ViewBag.Active = false;
            if (elof != null)
            {
                if (elof.LejarDatum > DateTime.Now)
                {
                    ViewBag.Active = true;
                }
            }

            return View(manager.GetKonyvek());

        }

        
        public ActionResult Theme(string theme)
        {
            ContentManager manager = new ContentManager();
            //Aktiv-e az előfizetés
            UserManager usermanager = new UserManager();
            Elofizetesek elof = usermanager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId);
            ViewBag.Active = false;
            if (elof != null)
            {
                if (elof.LejarDatum > DateTime.Now)
                {
                    ViewBag.Active = true;
                }
            }

            return View("Index", manager.GetKonyvek().Where(t=>t.tema == theme).ToList());
        }

        [Authorizes]
        public ActionResult Read(string isbn)
        {
            UserManager usermanager = new UserManager();
            Elofizetesek elof = usermanager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId);
            if (elof != null)
            {
                if (elof.LejarDatum > DateTime.Now)
                {
                    ContentManager manager = new ContentManager();

                    return View(manager.GetKonyv(isbn));
                }
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
