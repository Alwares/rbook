﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using rBook.Managers;

namespace rBook.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/

        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult Index(String query)
        {
            ContentManager manager = new ContentManager();
            SearchContent content = manager.Search(query);
            return View(content);
        }
    }
}
