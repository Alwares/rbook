﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using rBook.Managers;
using rBook.Models;

namespace rBook.Controllers
{
    public class MovieController : Controller
    {
        //
        // GET: /Movie/

        public ActionResult Index()
        {
            ContentManager manager = new ContentManager();
            //Aktiv-e az előfizetés
            UserManager usermanager = new UserManager();
            Elofizetesek elof = usermanager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId);
            ViewBag.Active = false;
            if (elof != null)
            {
                if (elof.LejarDatum > DateTime.Now)
                {
                    ViewBag.Active = true;
                }
            }

            return View(manager.GetFilmek());
        }

        public ActionResult Theme(string theme)
        {
            ContentManager manager = new ContentManager();
            //Aktiv-e az előfizetés
            UserManager usermanager = new UserManager();
            Elofizetesek elof = usermanager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId);
            ViewBag.Active = false;
            if (elof != null)
            {
                if (elof.LejarDatum > DateTime.Now)
                {
                    ViewBag.Active = true;
                }
            }

            return View("Index", manager.GetFilmek().Where(t => t.tema == theme).ToList());
        }

        public ActionResult Watch(int filmid)
        {
            ContentManager manager = new ContentManager();
            //Aktiv-e az előfizetés
            UserManager usermanager = new UserManager();
            Elofizetesek elof = usermanager.GetLastestSubscribe(CurrentData.CurrentUser.UserIdentity.User.FelhasznaloId);
            ViewBag.Active = false;
            if (elof != null)
            {
                if (elof.LejarDatum > DateTime.Now)
                {
                    ViewBag.Active = true;
                }
            }

            return View(manager.GetFilm(filmid));
        }
    }
}
