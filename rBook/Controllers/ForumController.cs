﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using rBook.Managers;

namespace rBook.Controllers
{
    public class ForumController : Controller
    {
        //
        // GET: /Forum/

        public ActionResult Index()
        {
            ForumManager manager = new ForumManager();
            List<Models.Topic> topics = manager.GetTopics();
            return View(topics);
        }

        [Authorizes]
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [Authorizes]
        [HttpPost]
        public ActionResult Create(string TopicName)
        {
            ForumManager manager = new ForumManager();
            manager.CreateTopic(TopicName);
            return RedirectToAction("Index");
        }

        public ActionResult Topic(string topic)
        {
            ForumManager manager = new ForumManager();
            ViewBag.Topic = manager.GetTopic(int.Parse(topic));
            return View(manager.GetPosts(int.Parse(topic)));
        }

        [Authorizes]
        [HttpPost]
        public ActionResult Send(string message,string topicId)
        {
            ForumManager manager = new ForumManager();
            manager.SendPost(int.Parse(topicId), message);

            return RedirectToAction("Topic", new { topic = topicId });
        }
    }
}
