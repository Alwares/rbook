/***********************************************
* Random tartalom a headerben
***********************************************/

if (document.getElementById)
document.documentElement.className = 'jsclass'; 


var randomcontentdisplay={
	divholders:new Object(),
	masterclass: "randomcontent",

	init:function(){
		if (!document.getElementById)
			return
		var alldivs=document.getElementsByTagName("div")
		var randomcontentsearch=new RegExp(this.masterclass+"\\s+(group\\d+)", "i") //check CSS
		for (var i=0; i<alldivs.length; i++){
			if (randomcontentsearch.test(alldivs[i].className)){
				if (typeof this.divholders[RegExp.$1]=="undefined") //ha még nincs div
					this.divholders[RegExp.$1]=new Array() //tömb készítés 1.
					this.divholders[RegExp.$1].push(alldivs[i]) //ezt a divet a tömbbe
			}
		}
	this.showone()
	},

	showone:function(){
		for (group in this.divholders){ //objektumon belül összes tömbön végigloopolás
			var chosenOne=Math.floor(Math.random()*this.divholders[group].length) //egy random bejegyzés választása a tömbből
			this.divholders[group][chosenOne].style.display="block" //a kiválasztott elemhez tartozó tartalom megjelenítése
		}
	}
}
