﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using rBook.Models;

namespace rBook
{
    public static class DataContextFactory
    {
        [ThreadStatic]
        private static RbookEntities instance;

        public static RbookEntities GetInstance()
        {
            if (instance == null)
            {
                instance = new RbookEntities();
            }
            return instance;
        }

        public static void CleanUp()
        {
            if (instance != null)
            {
                instance.Dispose();
            }
            instance = null;
        }
    }
}