﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Szimedok.Models;
//using Szimedok.Helpers;

namespace rBook
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class Authorizes : AuthorizeAttribute
    {
        //Felhasználónak nincs jogosultsága az adott művelet végrehajtásához, de nem ismeretlen.
        private bool DontHaveRight;
        //private string CurrentRight { get { } set { } }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (AuthorizeCore(filterContext.HttpContext))
            {
                if (DontHaveRight)
                {
                    HandleUnauthorizedRequest(filterContext);
                    //Ha a felhasználó be van jelentkezve és olyan műveletet akar végrehajtani amihez nincs jogosultsága
                    filterContext.Result = new RedirectResult("/Account/AccesDenied");
                        //System.Web.Security.FormsAuthentication.LoginUrl + "?returnUrl=" +
                    //filterContext.HttpContext.Server.UrlEncode(filterContext.HttpContext.Request.RawUrl));
                }
                else
                {
                    HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
                    cachePolicy.SetProxyMaxAge(new TimeSpan(0));
                }
                //cachePolicy.AddValidationCallback(CacheValidateHandler, null /* data */);
            }
            else
            {
                HandleUnauthorizedRequest(filterContext);
                //Ha a felhasználó nincs bejelentkezve else{ ha a felhasználó olyan oldalt kér amihez nincs jogsultsága
                filterContext.Result = new RedirectResult(System.Web.Security.FormsAuthentication.LoginUrl + "?returnUrl=" +
                filterContext.HttpContext.Server.UrlEncode(filterContext.HttpContext.Request.RawUrl));
            }
            filterContext.HttpContext.User = (HttpContext.Current.Session["user"] as UserPrincipal);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            httpContext.User = CurrentData.CurrentUser;
            if (CurrentData.CurrentUser.UserIdentity.IsAuthenticated)
            {
                return true;
                //if (Roles.ToString() != "")
                //{
                //    if (CurrentData.CurrentUser.UserIdentity.IsUserHasRight(Roles)) //(HttpContext.Current.Session["user"] as UserPrincipal).UserIdentity.IsUserHasRight(Right.ToString()))
                //    {
                //        DontHaveRight = false;
                //        return true;
                //    }
                //    else
                //    {
                //        DontHaveRight = true;
                //        return true;
                //    }
                //}
                //else
                //{
                //    return true;
                //}
            }
            else
            {
                return false;
            }
        }

    }
}