﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using rBook.Models;

namespace rBook
{
    public class ManagerBase
    {
        private RbookEntities _dataContext;

        public RbookEntities DataContext
        {
            get
            {
                if (this._dataContext != null)
                {
                    return this._dataContext;
                }
                else
                {
                    return DataContextFactory.GetInstance();
                }
            }
            set { this._dataContext = value; }
        }

        public static void CleanUp()
        {
            DataContextFactory.CleanUp();
        }

    }



}