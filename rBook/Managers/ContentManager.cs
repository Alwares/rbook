﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using rBook.Models;

namespace rBook.Managers
{
    public class ContentManager : ManagerBase
    {
        public Konyvek GetKonyv(string isbn)
        {
            return this.DataContext.Konyvek.Where(k => k.isbn == isbn).FirstOrDefault();
        }

        public List<Konyvek> GetKonyvek()
        {
            return this.DataContext.Konyvek.ToList();
        }

        public List<Filmek> GetFilmek()
        {
            return this.DataContext.Filmek.ToList();
        }

        public Filmek GetFilm(int filmid)
        {
            return this.DataContext.Filmek.Where(f => f.filmId == filmid).FirstOrDefault();
        }

        /// <summary>
        /// Ha a könyv tartalmazza a töredéket, visszaadja listában
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public SearchContent Search(string name)
        {
            SearchContent res = new SearchContent();
            res.Konyvek = new List<Konyvek>();
            this.DataContext.Konyvek.Where(k => k.cim.ToLower().StartsWith(name.ToLower())).ToList().ForEach(ksw => res.Konyvek.Add(ksw));
            foreach (Konyvek konyv in this.DataContext.Konyvek.Where(k => k.cim.ToLower().Contains(name.ToLower())).ToList())
            {
                if (!res.Konyvek.Contains(konyv))
                {
                    res.Konyvek.Add(konyv);
                }
            }

            res.Filmek = new List<Filmek>();
            this.DataContext.Filmek.Where(k => k.cim.ToLower().StartsWith(name.ToLower())).ToList().ForEach(ksw => res.Filmek.Add(ksw));
            foreach (Filmek film in this.DataContext.Filmek.Where(k => k.cim.ToLower().Contains(name.ToLower())).ToList())
            {
                if (!res.Filmek.Contains(film))
                {
                    res.Filmek.Add(film);
                }
            }

            return res;
        }

    }
    public class SearchContent
    {
        public List<Konyvek> Konyvek { get; set; }
        public List<Filmek> Filmek { get; set; }
    }
}