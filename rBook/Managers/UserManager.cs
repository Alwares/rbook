﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using rBook.Models;

namespace rBook
{
    public class UserManager : ManagerBase
    {
        public Felhasznalok GetUser(int id)
        {
            return this.DataContext.Felhasznalok.Single(u => u.FelhasznaloId == id);
        }

        public List<Felhasznalok> GetUsers()
        {
            return this.DataContext.Felhasznalok.ToList();
        }

        public bool ValidateUser(string email, string password)
        {
            if (this.DataContext.Felhasznalok.Any(f => f.Email == email && f.Password == password))
            {
                CurrentData.UserData = this.DataContext.Felhasznalok.Where(f => f.Email == email && f.Password == password).FirstOrDefault();
                CurrentData.CurrentUser.UserIdentity.ValidateUser(CurrentData.UserData);
                return true;
            }
            else
            {
                CurrentData.UserData = null;
                return false;
            }
        }

        public void RegisterUser(RegisterModel regmodel)
        {
            Felhasznalok user = new Felhasznalok();
            user.UserName = regmodel.UserName;
            user.Email = regmodel.Email;
            user.Password = regmodel.Password;
            this.DataContext.AcceptAllChanges();
            this.DataContext.Felhasznalok.AddObject(user);
            this.DataContext.SaveChanges();
            
        }

        public Elofizetesek GetLastestSubscribe(int userId)
        {
            return this.DataContext.Elofizetesek.Where(e => e.FelhasznaloId.Value == userId).OrderByDescending(o => o.LejarDatum).FirstOrDefault();
        }

        public void SetSubscribe(int userId)
        {
            if (this.DataContext.Elofizetesek.Any(e => e.FelhasznaloId.Value == userId))
            {
                Elofizetesek elof = new Elofizetesek();
                elof.FelhasznaloId = userId;
                elof.KezdDatum = this.DataContext.Elofizetesek.Where(e => e.FelhasznaloId.Value == userId).OrderByDescending(o => o.LejarDatum).FirstOrDefault().LejarDatum;
                elof.LejarDatum = elof.KezdDatum.AddDays(30);
                elof.Felhasznalok = GetUser(userId);

                this.DataContext.AcceptAllChanges();
                this.DataContext.Elofizetesek.AddObject(elof);
                this.DataContext.SaveChanges();
            }
            else
            {
                Elofizetesek elof = new Elofizetesek();
                elof.FelhasznaloId = userId;
                elof.KezdDatum = DateTime.Now;
                elof.LejarDatum = elof.KezdDatum.AddDays(30);
                elof.Felhasznalok = GetUser(userId);

                
                this.DataContext.Elofizetesek.AddObject(elof);
                this.DataContext.SaveChanges();
            }
        }
    }
}