﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Web.Security;
using rBook.Models;

namespace rBook
{
    public class UserIdentity : IIdentity
    {
        #region IoC

        private UserManager _userManager;
        public UserManager UserManager
        {
            get
            {
                if (this._userManager == null)
                {
                    this._userManager = new UserManager();
                }
                return this._userManager;
            }
            set { this._userManager = value; }
        }
        #endregion
        public UserIdentity()
        {
            User = new Models.Felhasznalok();
            IsUserSet = false;
        }
        //A user általános (adatbázisból elérhető) adatai
        private Felhasznalok user { get; set; }
        private int UserId { get; set; }
        public Felhasznalok User
        {
            get
            {
                if (user == null)
                user = new Felhasznalok();
            return user;
            }
            set { user = value; }
        }
        public bool IsUserSet { get; set; }

        public string AuthenticationType
        {
            get { return "User"; }
        }

        public bool IsAuthenticated
        {
            get { return IsUserSet; }
        }
        public bool ValidateUser(Felhasznalok fsz)
        {
            //Hasonló metódus, többszörös hivatkozás, lehet nem szükséges, ezt még át kell gondolni/dolgozni.
                //if (UserManager.ValidateUser(email, passWord))
                //{

                    this.User = fsz;
                    FormsAuthentication.SetAuthCookie(this.User.FelhasznaloId.ToString(), true);
                    HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                    if (authCookie != null)
                    {
                        FormsAuthenticationTicket authTicket = null;
                        authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                        if (authTicket != null && !authTicket.Expired)
                        {
                            FormsAuthenticationTicket newAuthTicket = authTicket;

                            if (FormsAuthentication.SlidingExpiration)
                            {
                                newAuthTicket = FormsAuthentication.RenewTicketIfOld(authTicket);
                            }
                            string userData = newAuthTicket.UserData;
                            string[] roles = userData.Split(',');
                            System.Web.HttpContext.Current.User =
                                new System.Security.Principal.GenericPrincipal(new FormsIdentity(newAuthTicket), roles);
                        }
                    }
                    IsUserSet = true;
                    return true;
                //}
                //else
                //{
                //    this.User = new Felhasznalok();
                //    IsUserSet = false;
                //    return false;
                //}
        }

        // Usernek megvan-e a paraméterben adott joga. A right paraméter a jog nevét várja
        // (névvel egyszerűbb azonosítás, a jognak úgyis egyedi nevűnek kell lennie

        public string Name
        {
            get { return User.UserName; } //???
        }
    }
    public class UserPrincipal : IPrincipal
    {
        public UserPrincipal(UserIdentity user)
        {
            this.Identity = user;
            UserIdentity = user;
        }
        /// <summary>
        /// Ezen keresztül lehet lekérdezni az akutális User különféle adatait, jogait, szerepköreit stb
        /// </summary>
        public UserIdentity UserIdentity { get; set; }

        /// <summary>
        /// Az IPrincipal interface megvalósítása sok helyen használva van.
        /// </summary>
        public IIdentity Identity
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Az IsInRole esetünkben valójában nem role-t vizsgál, hanem rightot, csak a IPrincipal interface megvalósítása miatt kell ezt
        /// a metódust használni.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsInRole(string right)
        {
            return true;
        }
    }
}