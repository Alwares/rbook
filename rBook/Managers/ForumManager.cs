﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using rBook.Models;
using System.Collections.Specialized;

namespace rBook.Managers
{
    public class ForumManager : ManagerBase
    {
        
        public List<Topic> GetTopics()
        {
            UserManager userManager = new UserManager();
            List<Topic> topics = this.DataContext.Topic.ToList();
            for(int i=0;i<topics.Count;i++)
            {
                topics[i].Felhasznalok = userManager.GetUser(topics[i].Owner.Value);
                if (!topics[i].LostPost.HasValue)
                    topics[i].LastPost = new Felhasznalok();
                else
                    topics[i].LastPost = userManager.GetUser(topics[i].LostPost.Value);
            }
            return topics;
        }

        public void CreateTopic(string topicName)
        {
            UserManager userManager = new UserManager();

            Topic thread = new Topic();
            thread.Name = topicName;
            thread.Felhasznalok = userManager.GetUser(CurrentData.UserData.FelhasznaloId);
            thread.Owner = CurrentData.UserData.FelhasznaloId;
            thread.Created = DateTime.Now;

            this.DataContext.AddToTopic(thread);
            this.DataContext.SaveChanges();
        }

        public List<Posts> GetPosts(int id)
        {
            UserManager userManager = new UserManager();
            List<Posts> posts = new List<Posts>();
            posts = this.DataContext.Posts.Where(p => p.Topic == id).OrderByDescending(o=>o.Created).ToList();
            for (int i = 0; i < posts.Count; i++)
            {
                posts[i].Felhasznalok = userManager.GetUser(posts[i].Owner);
            }
            return posts;
        }

        public Topic GetTopic(int id)
        {
            return this.DataContext.Topic.Where(t => t.id == id).FirstOrDefault();
        }

        public void SendPost(int topicId,string message)
        {
            UserManager userManager = new UserManager();
            this.DataContext.AddToPosts(new Posts() {
                  Created = DateTime.Now,
                  Felhasznalok = userManager.GetUser(CurrentData.UserData.FelhasznaloId),
                  Text = message,
                  Topic = topicId
            });
            this.DataContext.SaveChanges();

            Topic topic = GetTopic(topicId);
            topic.LastPost = this.DataContext.Posts.Where(p => p.Topic == topicId).OrderByDescending(o => o.Created).FirstOrDefault().Felhasznalok;
            topic.LastPostDate = this.DataContext.Posts.Where(p => p.Topic == topicId).OrderByDescending(o => o.Created).FirstOrDefault().Created;
            //this.DataContext.AcceptAllChanges();
            //this.DataContext.Topic.(topic);
            this.DataContext.SaveChanges();
        }

    }
}