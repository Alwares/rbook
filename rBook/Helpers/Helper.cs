﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rBook
{
    public static class CurrentData
    {
        public static UserPrincipal CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session["user"] == null)
                    HttpContext.Current.Session.Add("user", new UserPrincipal(new UserIdentity()));
                return (HttpContext.Current.Session["user"] as UserPrincipal);
            }
            set { HttpContext.Current.Session["user"] = value; }
        }
        //Az adatbázisból érkező useradatok ebben a változóban vannak tárolva
        public static Models.Felhasznalok UserData
        {
            get
            {
                return (HttpContext.Current.Session["user"] as UserPrincipal).UserIdentity.User;
            }
            set { (HttpContext.Current.Session["user"] as UserPrincipal).UserIdentity.User = value; }
        }
    }
}